#include "max31865.h"
#include "mbed.h"

max31865::max31865(PinName MOSI, PinName MISO, PinName SCLK, PinName CS) : spi(MOSI, MISO, SCLK), cs(CS) // mosi, miso, sclk
{
    //constructor
    cs = 1; //deselect chip
    spi.format(8,1); //set mode 1 and 8 bit
    spi.frequency(100000); //set frequency to 0.5mhz
}

int max31865::ReadRTD()
{
    ClearFault();
    EnableBias(true);
    Enable50HzFilter(true);

    int t = ReadRegistor8(MAX31856_CONFIG_REG);
    t |= MAX31856_CONFIG_1SHOT;
    t |= MAX31856_CONFIG_FILT50HZ;

    WriteRegistor(MAX31856_CONFIG_REG, t);

    wait_ns(65000000);

    int RTD = ReadRegistor16(MAX31856_RTDMSB_REG);
    printf("RTD %d\r\n", RTD);

    // remove fault
    RTD >>= 1;

    return RTD;
}

void max31865::Begin(max31865_numwires_t wires)
{
    cs = 1;
    SetWires(wires);
    EnableBias(false);
    AutoConvert(false);
    ClearFault();
}

int max31865::ReadFault()
{
    return ReadRegistor8(MAX31856_FAULTSTAT_REG);
}

void max31865::ClearFault()
{
    int t = ReadRegistor8(MAX31856_CONFIG_REG);
    t &= ~0x2C;
    t |= MAX31856_CONFIG_FAULTSTAT;
    WriteRegistor(MAX31856_CONFIG_REG, t);
}

void max31865::EnableBias(bool b)
{
    int t = ReadRegistor8(MAX31856_CONFIG_REG);
    if (b)
    {
        t |= MAX31856_CONFIG_BIAS;       // enable bias
    }
    else
    {
        t &= ~MAX31856_CONFIG_BIAS;       // disable bias
    }
    WriteRegistor(MAX31856_CONFIG_REG, t);
}

void max31865::Enable50HzFilter(bool b)
{
    int t = ReadRegistor8(MAX31856_CONFIG_REG);
    if (b)
    {
        t |= MAX31856_CONFIG_FILT50HZ;       // enable bias
    }
    else
    {
        t &= ~MAX31856_CONFIG_FILT50HZ;       // disable bias
    }
    WriteRegistor(MAX31856_CONFIG_REG, t);
}

void max31865::AutoConvert(bool b)
{
    int t = ReadRegistor8(MAX31856_CONFIG_REG);
    if (b)
    {
        t |= MAX31856_CONFIG_MODEAUTO;       // enable autoconvert
    }
    else
    {
        t &= ~MAX31856_CONFIG_MODEAUTO;       // disable autoconvert
    }
    WriteRegistor(MAX31856_CONFIG_REG, t);
}

void max31865::SetWires(max31865_numwires_t wires )
{
    int t = ReadRegistor8(MAX31856_CONFIG_REG);

    if (wires == MAX31865_3WIRE)
    {
        t |= MAX31856_CONFIG_3WIRE;
    }
    else
    {
        // 2 or 4 wire
        t &= ~MAX31856_CONFIG_3WIRE;
    }
    WriteRegistor(MAX31856_CONFIG_REG, t);
}


int max31865::ReadRegistor8(int address)
{
    int ret = 0;
    ReadRegistorN(address, &ret, 1);
    return ret;
}

int max31865::ReadRegistor16(int address)
{
    int buffer[2] = {0, 0};
    ReadRegistorN(address, buffer, 2);

    int ret = buffer[0];
    ret <<= 8;
    ret |=  buffer[1];

    return ret;
}

void max31865::ReadRegistorN(int address, int buffer[], int n)
{
    address &= 0x7F; // make sure top bit is not set

    cs = 0;

    spiXfer(address);

    while(n--)
    {
        buffer[0] = spiXfer(0xFF);
        buffer++;
    }

    cs = 1;
}

void max31865::WriteRegistor(int address, int data)
{

    cs = 0; //select chip
    spiXfer(address | 0x80);   // make sure top bit is set
    spiXfer(data);

    cs = 1;
}

int max31865::spiXfer(int x)
{
    return spi.write(x);
}
#define RTD_A 3.9083e-3
#define RTD_B -5.775e-7

float  max31865::temperature() {
  // http://www.analog.com/media/en/technical-documentation/application-notes/AN709_0.pdf

  float temp;

  while (true)
  {
      ClearFault();
      float Z1, Z2, Z3, Z4, Rt;
      float refResistor = 430;
      float RTDnominal = 100;

      Rt = ReadRTD();

      Rt /= 32768;
      Rt *= refResistor;
      printf("Resistance: %1.4f\r\n", Rt);

      Z1 = -RTD_A;
      Z2 = RTD_A * RTD_A - (4 * RTD_B);
      Z3 = (4 * RTD_B) / RTDnominal;
      Z4 = 2 * RTD_B;

      temp = Z2 + (Z3 * Rt);
      temp = (sqrt(temp) + Z1) / Z4;

      if (temp > 0.0 && temp < 200.0)
      {
          return temp;
      }
      else
      {
          Begin();
          continue;
      }

      // ugh.
      float rpoly = Rt;

      temp = -242.02f;
      temp += 2.2228f * rpoly;
      rpoly *= Rt;  // square
      temp += (float) 2.5859e-3 * rpoly;
      rpoly *= Rt;  // ^3
      temp -= (float) 4.8260e-6 * rpoly;
      rpoly *= Rt;  // ^4
      temp -= (float) 2.8183e-8 * rpoly;
      rpoly *= Rt;  // ^5
      temp += (float) 1.5243e-10 * rpoly;

      if (temp > -20.0 && temp < 200.0)
      {
          return temp;
      }
      else
      {
          Begin();
      }

    }

}
