#ifndef DISPLAY_H
#define DISPLAY_H

#include <stdint.h>

void main_menu(uint8_t selection);
void solar_pump(uint8_t selection);
void solar_pump_settings(uint8_t selection);
void warm_water_valve(uint8_t selection);
void warm_water_valve_settings(uint8_t selection);
void louvre_settings(uint8_t selection);
void regulator_valve(uint8_t selection);
void regulator_valve_settings(uint8_t selection);
void sensor_values();
void louvre(uint8_t selection);


#endif
