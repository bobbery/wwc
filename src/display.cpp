#include "display.h"
#include "LCD_DISCO_F429ZI.h"
#include "myconfig.h"
#include "tempValues.h"

LCD_DISCO_F429ZI lcd;
extern TempValues tempValues;

void drawSelection(uint8_t line)
{
    lcd.SetTextColor(LCD_COLOR_RED);
    lcd.DrawRect(5, LINE(line), 225, LINE(1));
    lcd.DrawRect(5-1, LINE(line)-1, 225+2, LINE(1)+2);
    lcd.DrawRect(5-2, LINE(line)-2, 225+4, LINE(1)+4);
}

void drawAutomatic(uint8_t line)
{
    lcd.SetTextColor(LCD_COLOR_BLACK);
    lcd.DrawRect(5, LINE(line) - 2, 225, LINE(1) + 2);
    lcd.SetTextColor(LCD_COLOR_BLACK);
}


void main_menu(uint8_t selection)
{
    const uint8_t positions[] = {2, 5, 8, 11, 14};
    BSP_LCD_SetFont(&Font20);
    lcd.Clear(LCD_COLOR_WHITE);
    lcd.SetBackColor(LCD_COLOR_WHITE);
    lcd.SetTextColor(LCD_COLOR_BLACK);

    if (config.solarPump == Config::ON || tempValues.solarPumpOn)
    {
        lcd.SetBackColor(LCD_COLOR_GREEN);
    }
    else if (config.solarPump == Config::AUTO)
    {
        drawAutomatic(2);
    }

    lcd.DisplayStringAt(0, LINE(2), (uint8_t *)"Solar-Pumpe" , CENTER_MODE);

    lcd.SetBackColor(LCD_COLOR_WHITE);

    if (config.regulatorValve == Config::ON || tempValues.regulatorValveOpen)
    {
        lcd.SetBackColor(LCD_COLOR_GREEN);
    }
    else if (config.regulatorValve == Config::AUTO)
    {
        drawAutomatic(5);
    }

    lcd.DisplayStringAt(0, LINE(5), (uint8_t *)"Regel-Ventil" , CENTER_MODE);

    lcd.SetBackColor(LCD_COLOR_WHITE);

    if (config.warmWaterValve == Config::ON || tempValues.warmWaterValveOn)
    {
        lcd.SetBackColor(LCD_COLOR_GREEN);
    }
    else if (config.warmWaterValve == Config::AUTO)
    {
        drawAutomatic(8);
    }

    lcd.DisplayStringAt(0, LINE(8), (uint8_t *)"WW-Ventil" , CENTER_MODE);

    lcd.SetBackColor(LCD_COLOR_WHITE);

    if (config.louvre == Config::ON)
    {
        lcd.SetBackColor(LCD_COLOR_GREEN);
    }

    if (config.louvre == Config::OFF)
    {
        lcd.SetBackColor(LCD_COLOR_RED);
    }

    if (config.louvre == Config::AUTO)
    {
        if (tempValues.louvreClosed)
        {
            lcd.SetBackColor(LCD_COLOR_RED);
        }
        else if (tempValues.louvreOpen)
        {
            lcd.SetBackColor(LCD_COLOR_GREEN);
        }
        drawAutomatic(11);
    }

    lcd.DisplayStringAt(0, LINE(11), (uint8_t *)"Jalousie" , CENTER_MODE);

    lcd.SetBackColor(LCD_COLOR_WHITE);

    lcd.DisplayStringAt(0, LINE(14), (uint8_t *)"Messwerte" , CENTER_MODE);

    drawSelection(positions[selection]);
}

void solar_pump(uint8_t selection)
{
    const uint8_t positions[] = {2, 5, 8, 11};
    BSP_LCD_SetFont(&Font24);
    lcd.Clear(LCD_COLOR_WHITE);
    lcd.SetBackColor(LCD_COLOR_WHITE);
    lcd.SetTextColor(LCD_COLOR_BLACK);
    lcd.DisplayStringAt(0, LINE(2), (uint8_t *)"Ein" , CENTER_MODE);
    lcd.DisplayStringAt(0, LINE(5), (uint8_t *)"Aus" , CENTER_MODE);
    lcd.DisplayStringAt(0, LINE(8), (uint8_t *)"Auto" , CENTER_MODE);
    lcd.DisplayStringAt(0, LINE(11), (uint8_t *)"Einstellungen" , CENTER_MODE);

    drawSelection(positions[selection]);
}


void solar_pump_settings(uint8_t selection)
{
    const uint8_t positions[] = {2, 5, 8};
    BSP_LCD_SetFont(&Font24);
    lcd.Clear(LCD_COLOR_WHITE);
    lcd.SetBackColor(LCD_COLOR_WHITE);
    lcd.SetTextColor(LCD_COLOR_BLACK);

    char buf[20];
    sprintf(buf, "Dein = %d C", config.solarPumpDeltaOn);
    lcd.DisplayStringAt(0, LINE(2), (uint8_t *)buf, CENTER_MODE);

    sprintf(buf, "Daus = %d C", config.solarPumpDeltaOff);
    lcd.DisplayStringAt(0, LINE(5), (uint8_t *)buf, CENTER_MODE);
    lcd.DisplayStringAt(0, LINE(8), (uint8_t *)"Speichern" , CENTER_MODE);

    drawSelection(positions[selection]);
}

void warm_water_valve(uint8_t selection)
{
    const uint8_t positions[] = {2, 5, 8, 11};
    BSP_LCD_SetFont(&Font24);
    lcd.Clear(LCD_COLOR_WHITE);
    lcd.SetBackColor(LCD_COLOR_WHITE);
    lcd.SetTextColor(LCD_COLOR_BLACK);
    lcd.DisplayStringAt(0, LINE(2), (uint8_t *)"Ein" , CENTER_MODE);
    lcd.DisplayStringAt(0, LINE(5), (uint8_t *)"Aus" , CENTER_MODE);
    lcd.DisplayStringAt(0, LINE(8), (uint8_t *)"Auto" , CENTER_MODE);
    lcd.DisplayStringAt(0, LINE(11), (uint8_t *)"Einstellungen" , CENTER_MODE);

    drawSelection(positions[selection]);
}


void warm_water_valve_settings(uint8_t selection)
{
    const uint8_t positions[] = {2, 5, 8};
    BSP_LCD_SetFont(&Font24);
    lcd.Clear(LCD_COLOR_WHITE);
    lcd.SetBackColor(LCD_COLOR_WHITE);
    lcd.SetTextColor(LCD_COLOR_BLACK);

    char buf[20];
    sprintf(buf, "Tein = %d C", config.warmWaterValveOn);
    lcd.DisplayStringAt(0, LINE(2), (uint8_t *)buf, CENTER_MODE);
    sprintf(buf, "Delay = %d s", config.warmWaterValveDelay);
    lcd.DisplayStringAt(0, LINE(5), (uint8_t *)buf, CENTER_MODE);
    lcd.DisplayStringAt(0, LINE(8), (uint8_t *)"Speichern" , CENTER_MODE);

    drawSelection(positions[selection]);
}

void louvre(uint8_t selection)
{
    const uint8_t positions[] = {2, 5, 8, 11};
    BSP_LCD_SetFont(&Font24);
    lcd.Clear(LCD_COLOR_WHITE);
    lcd.SetBackColor(LCD_COLOR_WHITE);
    lcd.SetTextColor(LCD_COLOR_BLACK);
    lcd.DisplayStringAt(0, LINE(2), (uint8_t *)"Auf" , CENTER_MODE);
    lcd.DisplayStringAt(0, LINE(5), (uint8_t *)"Zu" , CENTER_MODE);
    lcd.DisplayStringAt(0, LINE(8), (uint8_t *)"Auto" , CENTER_MODE);
    lcd.DisplayStringAt(0, LINE(11), (uint8_t *)"Einstellungen" , CENTER_MODE);

    drawSelection(positions[selection]);
}

void louvre_settings(uint8_t selection)
{
    const uint8_t positions[] = {2, 5, 8};
    BSP_LCD_SetFont(&Font24);
    lcd.Clear(LCD_COLOR_WHITE);
    lcd.SetBackColor(LCD_COLOR_WHITE);
    lcd.SetTextColor(LCD_COLOR_BLACK);

    char buf[20];
    sprintf(buf, "Tzu = %d C", config.louvreTempClose);
    lcd.DisplayStringAt(0, LINE(2), (uint8_t *)buf, CENTER_MODE);

    sprintf(buf, "Tauf = %d C", config.louvreTempOpen);
    lcd.DisplayStringAt(0, LINE(5), (uint8_t *)buf, CENTER_MODE);
    lcd.DisplayStringAt(0, LINE(8), (uint8_t *)"Speichern" , CENTER_MODE);

    drawSelection(positions[selection]);
}

void regulator_valve(uint8_t selection)
{
    const uint8_t positions[] = {1, 3, 5, 8, 11};
    BSP_LCD_SetFont(&Font24);
    lcd.Clear(LCD_COLOR_WHITE);
    lcd.SetBackColor(LCD_COLOR_WHITE);
    lcd.SetTextColor(LCD_COLOR_BLACK);
    lcd.DisplayStringAt(0, LINE(1), (uint8_t *)"Auf" , CENTER_MODE);
    lcd.DisplayStringAt(0, LINE(3), (uint8_t *)"Zu" , CENTER_MODE);
    lcd.DisplayStringAt(0, LINE(5), (uint8_t *)"Zurueck" , CENTER_MODE);
    lcd.DisplayStringAt(0, LINE(8), (uint8_t *)"Auto" , CENTER_MODE);
    lcd.DisplayStringAt(0, LINE(11), (uint8_t *)"Einstellungen" , CENTER_MODE);

    drawSelection(positions[selection]);
}


void regulator_valve_settings(uint8_t selection)
{
    const uint8_t positions[] = {2, 8};
    BSP_LCD_SetFont(&Font24);
    lcd.Clear(LCD_COLOR_WHITE);
    lcd.SetBackColor(LCD_COLOR_WHITE);
    lcd.SetTextColor(LCD_COLOR_BLACK);

    char buf[20];
    sprintf(buf, "Tein = %d C", config.regulatorValveOn);
    lcd.DisplayStringAt(0, LINE(2), (uint8_t *)buf, CENTER_MODE);

    lcd.DisplayStringAt(0, LINE(8), (uint8_t *)"Speichern" , CENTER_MODE);

    drawSelection(positions[selection]);
}

void sensor_values()
{
    BSP_LCD_SetFont(&Font20);
    lcd.Clear(LCD_COLOR_WHITE);
    lcd.SetBackColor(LCD_COLOR_WHITE);
    lcd.SetTextColor(LCD_COLOR_BLACK);

    char buf[20];
    sprintf(buf, "Kollektor = %d C", tempValues.kollektor);
    lcd.DisplayStringAt(0, LINE(2), (uint8_t *)buf, CENTER_MODE);

    sprintf(buf, "Speicher = %d C", tempValues.speicher);
    lcd.DisplayStringAt(0, LINE(5), (uint8_t *)buf, CENTER_MODE);

    sprintf(buf, "Mischer = %d C", tempValues.mischer);
    lcd.DisplayStringAt(0, LINE(8), (uint8_t *)buf, CENTER_MODE);

    sprintf(buf, "Tacho = %d", tempValues.tacho);
    lcd.DisplayStringAt(0, LINE(11), (uint8_t *)buf, CENTER_MODE);
}
