// Version with analog sensors

#include "mbed.h"
#include "DS1820.h"
#include "fsm.h"
#include "container.h"
#include "display.h"
#include "statemachine.h"
#include "tempValues.h"
#include "myconfig.h"
#include "EventQueue.h"
#include "max31865.h"
#include "EncoderIrq.h"

TempValues tempValues;
Config config;
bool shallSaveConfig = false;

//max31865(PinName MOSI, PinName MISO, PinName SCLK, PinName CS);
// max31865 probeKollektorPt100(PB_15, PB_14, PB_13, PF_6);
// max31865 probeSpeicherPt100(PB_15, PB_14, PB_13, PG_13);
// max31865 probeMischerPt100(PB_15, PB_14, PB_13, PG_14);

DS1820 probeKollektor(PA_5);

DigitalOut relSolarPump(PD_4);
DigitalOut relMixerOpen(PD_5);
DigitalOut relMixerClose(PE_2);

DigitalOut relWarmWater(PE_3);

DigitalOut relLouvreOpen(PE_4);
DigitalOut relLouvreClose(PE_5);

InterruptIn tacho(PC_8);
uint32_t tachoCounter = 0;
uint32_t tickCounter = 0;

EncoderIrq encoder(PC_12, PC_11);
DigitalIn button(PC_13);

EventQueue queue(32 * EVENTS_EVENT_SIZE);

MainMenu mainMenu;
SolarPump solarPump;
SolarPumpSettings solarPumpSettings;
WarmWaterValve warmWaterValve;
WarmWaterValveSettings warmWaterValveSettings;
Louvre louvreState;
LouvreSettings louvreSettings;
RegulatorValve regulatorValve;
RegulatorValveSettings regulatorValveSettings;
ValuesState valuesState;

etl::ifsm_state* stateList[StateId::NUMBER_OF_STATES] =
{
  &mainMenu, &solarPump, &solarPumpSettings, &warmWaterValve, &warmWaterValveSettings, &louvreState, &louvreSettings, &regulatorValve, &regulatorValveSettings, &valuesState
};

HeaterControl heaterControl(stateList, 10);

enum RelaisState
{
    ON = 0,
    OFF = 1,
};



void rotCw()
{

    heaterControl.receive(Next());

}

void rotCcw()
{

    heaterControl.receive(Prev());

}

void buttonPressed()
{
    heaterControl.receive(Select());
}


void controlSolarPump()
{
    static uint8_t value = OFF;

    int16_t diff = tempValues.kollektor - tempValues.speicher;

    if (config.solarPump == Config::ON)
    {
        value = ON;
    }
    else if (config.solarPump == Config::OFF)
    {
        value = OFF;
    }
    else if (config.solarPump == Config::AUTO)
    {
        if (diff >= config.solarPumpDeltaOn)
        {
            value = ON;
        }
        else if (diff <= config.solarPumpDeltaOff)
        {
            value = OFF;
        }
        else
        {
            value = value;
        }
    }

    tempValues.solarPumpOn = (value == ON);
    relSolarPump = value;
}


void controlWarmWaterValve()
{
    uint8_t value = OFF;

    int16_t temp = tempValues.speicher;

    if (config.warmWaterValve == Config::ON)
    {
        value = ON;
    }
    else if (config.warmWaterValve == Config::OFF)
    {
        value = OFF;
    }
    else if (config.warmWaterValve == Config::AUTO)
    {
        if (temp >= config.warmWaterValveOn)
        {
            value = ON;
        }
        else
        {
            value = OFF;
        }
    }

    tempValues.warmWaterValveOn = (value == ON);
    relWarmWater = value;
}

void controlRegulatorValve()
{
    int16_t temp = tempValues.mischer;

    static int counter = 0;
    counter++;

    // Wenn Speicher zu kalt - ganz auf.
    if (config.regulatorValve == Config::AUTO && tempValues.speicher <= config.regulatorValveOn)
    {
        relMixerOpen = ON;
        relMixerClose = OFF;
        tempValues.regulatorValveOpen = (relMixerOpen == ON) || (relMixerClose == ON);
        return;
    }

    if (config.regulatorValve == Config::AUTO && tempValues.speicher > (config.regulatorValveOn + 5))
    {
        if (tempValues.tacho > 5)
        {
            if (temp <= config.regulatorValveOn - 2)
            {
                if (counter % 4 == 0)
                {
                    relMixerOpen = ON;
                    relMixerClose = OFF;
                }
                else
                {
                    relMixerOpen = OFF;
                    relMixerClose = OFF;
                }
            }
            else if (temp >= config.regulatorValveOn + 2)
            {
                if (counter % 4 == 0)
                {
                    relMixerOpen = OFF;
                    relMixerClose = ON;
                }
                else
                {
                    relMixerOpen = OFF;
                    relMixerClose = OFF;
                }
            }
            else
            {
                relMixerOpen = OFF;
                relMixerClose = OFF;
            }
        }
        else
        {
            // =====================
            relMixerOpen = OFF;
            relMixerClose = OFF;
        }
    }

    tempValues.regulatorValveOpen = (relMixerOpen == ON) || (relMixerClose == ON);
}

void controlLouvre()
{
    int16_t temp = tempValues.speicher;

    if (config.louvre == Config::ON) // Open
    {
        tempValues.louvreClosed = false;
        relLouvreOpen = ON;
        relLouvreClose = OFF;
    }
    else if (config.louvre == Config::OFF) // Close
    {
        tempValues.louvreClosed = true;
        relLouvreOpen = OFF;
        relLouvreClose = ON;
    }
    else if (config.louvre == Config::AUTO)
    {
        if (temp >= config.louvreTempClose)
        {
            tempValues.louvreClosed = true;
            tempValues.louvreOpen = false;
            relLouvreOpen = OFF;
            relLouvreClose = ON;
        }
        else if (temp <= config.louvreTempOpen)
        {
            tempValues.louvreClosed = false;
            tempValues.louvreOpen = true;
            relLouvreOpen = ON;
            relLouvreClose = OFF;
        }
        else
        {
            relLouvreOpen = relLouvreOpen;
            relLouvreClose = relLouvreClose;
        }
    }
}

bool buttonReleased = true;

void handleEncoder() {
    if (buttonReleased) {
        if (button == 0) {
            buttonReleased = false;
            buttonPressed();
            return;
        }
    } else {
        if (button == 1) {
            buttonReleased = true;
        }
    }
    int dir = encoder.read();
    printf("DIR: %d\n", dir);
    if (dir < 0) {
        rotCcw();
    }
    if (dir > 0) {
        rotCw();
    }
}

void tick()
{



    printf("Kollektor: \n");
    float val1 = probeKollektor.read();
    probeKollektor.startConversion();
    tempValues.kollektor = val1;
    printf("Temperatur: %3.2f\n", val1);

    printf("Speicher: \n");
    float val2 = 0; //probeSpeicherPt100.temperature();
    tempValues.speicher = val2;
    printf("Temperatur: %3.2f\n", val2);

    printf("Mischer: \n");
    float val3 = 0; //probeMischerPt100.temperature();
    tempValues.mischer = val3;
    printf("Temperatur: %3.2f\n", val3);

    controlLouvre();
    controlSolarPump();
    controlWarmWaterValve();
    controlRegulatorValve();

    if (tickCounter % 5 == 0)
    {
        tempValues.tacho = tachoCounter;
        tachoCounter = 0;
    }
    tickCounter++;

    heaterControl.receive(NewValue());

    storeConfigWhenNecessary();
}


void tachoSignal()
{
    tachoCounter++;
}


int main()
{
    tacho.rise(tachoSignal);
    relMixerOpen = OFF;
    relMixerClose = OFF;
    relLouvreOpen = OFF;
    relLouvreClose = OFF;
    relWarmWater = OFF;
    relSolarPump = OFF;

    loadConfig();

    probeKollektor.begin();
    // probeKollektorPt100.Begin();
    // probeSpeicherPt100.Begin();
    // probeMischerPt100.Begin();

    heaterControl.start();


    queue.call_every(300, handleEncoder);
    queue.call_every(1000, tick);

    // encoder.attachROTCW(rotCw);
    //  encoder.attachROTCCW(rotCcw);
    //  encoder.attachSW(puttonPressed);

    queue.dispatch_forever();
}
