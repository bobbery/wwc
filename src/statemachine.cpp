#include "mbed.h"
#include "statemachine.h"
#include "display.h"
#include "myconfig.h"
#include "ETL/fsm.h"

extern DigitalOut relMixerOpen;
extern DigitalOut relMixerClose;


etl::fsm_state_id_t MainMenu::on_enter_state()
{
    main_menu(m_selection);
    //saveConfig();
    return STATE_ID;
}

etl::fsm_state_id_t MainMenu::on_event(etl::imessage_router &sender, const Next &event)
{
    m_selection++;
    if (m_selection > VALUES) m_selection = VALUES;

    main_menu(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t MainMenu::on_event(etl::imessage_router &sender, const Prev &event)
{
    m_selection--;
    if (m_selection < SOLAR_PUMP) m_selection = SOLAR_PUMP;

    main_menu(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t MainMenu::on_event(etl::imessage_router &sender, const Select &event)
{
    if (m_selection == SOLAR_PUMP)
    {
        return StateId::SOLAR_PUMP;
    }
    else if (m_selection == WARM_WATER_VALVE)
    {
        return StateId::WARM_WATER_VALVE;
    }
    else if (m_selection == LOUVRE)
    {
        return StateId::LOUVRE;
    }
    else if (m_selection == REGULATOR_VALVE)
    {
        return StateId::REGULATOR_VALVE;
    }
    else if (m_selection == VALUES)
    {
        return StateId::VALUES;
    }

    return STATE_ID;
}

etl::fsm_state_id_t MainMenu::on_event(etl::imessage_router &sender, const NewValue &event)
{
    main_menu(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t MainMenu::on_event_unknown(etl::imessage_router &sender, const etl::imessage &event)
{
    return STATE_ID;
}

MainMenu::MainMenu()
{
    m_selection = SOLAR_PUMP;
}

SolarPump::SolarPump()
{
    m_selection = AUTO;
}

etl::fsm_state_id_t SolarPump::on_enter_state()
{
    solar_pump(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t SolarPump::on_event(etl::imessage_router &sender, const Next &event)
{
    m_selection++;
    if (m_selection > SETTINGS) m_selection = SETTINGS;

    solar_pump(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t SolarPump::on_event(etl::imessage_router &sender, const Prev &event)
{
    m_selection--;
    if (m_selection < ON) m_selection = ON;

    solar_pump(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t SolarPump::on_event(etl::imessage_router &sender, const Select &event)
{
    if (m_selection == ON)
    {
        config.solarPump = Config::ON;
    }

    if (m_selection == OFF)
    {
        config.solarPump = Config::OFF;
    }

    if (m_selection == AUTO)
    {
        config.solarPump = Config::AUTO;
    }

    if (m_selection == SETTINGS)
    {
        return StateId::SOLAR_PUMP_SETTINGS;
    }

    return StateId::MAIN_MENU;
}

etl::fsm_state_id_t SolarPump::on_event_unknown(etl::imessage_router &sender, const etl::imessage &event)
{
    return STATE_ID;
}

SolarPumpSettings::SolarPumpSettings()
{
    m_selection = TON;
    m_substate = NONE;
}

etl::fsm_state_id_t SolarPumpSettings::on_enter_state()
{
    solar_pump_settings(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t SolarPumpSettings::on_event(etl::imessage_router &sender, const Next &event)
{
    if (m_substate == NONE)
    {
        m_selection++;
        if (m_selection > SAVE) m_selection = SAVE;
    }

    if (m_substate == ADJ_TON)
    {
        config.solarPumpDeltaOn++;
    }

    if (m_substate == ADJ_TOFF)
    {
        config.solarPumpDeltaOff++;
    }

    solar_pump_settings(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t SolarPumpSettings::on_event(etl::imessage_router &sender, const Prev &event)
{
    if (m_substate == NONE)
    {
        m_selection--;
        if (m_selection < TON) m_selection = TON;
    }

    if (m_substate == ADJ_TON)
    {
        config.solarPumpDeltaOn--;
    }

    if (m_substate == ADJ_TOFF)
    {
        config.solarPumpDeltaOff--;
    }

    solar_pump_settings(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t SolarPumpSettings::on_event(etl::imessage_router &sender, const Select &event)
{
    if (m_substate != NONE)
    {
        m_substate = NONE;
        return StateId::SOLAR_PUMP_SETTINGS;
    }

    if (m_selection == TON)
    {
        m_substate = ADJ_TON;
    }

    if (m_selection == TOFF)
    {
        m_substate = ADJ_TOFF;
    }

    if (m_selection == SAVE)
    {
        saveConfig();
        return StateId::SOLAR_PUMP;
    }


    return StateId::SOLAR_PUMP_SETTINGS;
}

etl::fsm_state_id_t SolarPumpSettings::on_event_unknown(etl::imessage_router &sender, const etl::imessage &event)
{
    return STATE_ID;
}

WarmWaterValve::WarmWaterValve()
{
    m_selection = AUTO;
}

etl::fsm_state_id_t WarmWaterValve::on_enter_state()
{
    warm_water_valve(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t WarmWaterValve::on_event(etl::imessage_router &sender, const Next &event)
{
    m_selection++;
    if (m_selection > SETTINGS) m_selection = SETTINGS;

    warm_water_valve(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t WarmWaterValve::on_event(etl::imessage_router &sender, const Prev &event)
{
    m_selection--;
    if (m_selection < ON) m_selection = ON;

    warm_water_valve(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t WarmWaterValve::on_event(etl::imessage_router &sender, const Select &event)
{
    if (m_selection == ON)
    {
        config.warmWaterValve = Config::ON;
    }

    if (m_selection == OFF)
    {
        config.warmWaterValve = Config::OFF;
    }

    if (m_selection == AUTO)
    {
        config.warmWaterValve = Config::AUTO;
    }

    if (m_selection == SETTINGS)
    {
        return StateId::WARM_WATER_VALVE_SETTINGS;
    }

    return StateId::MAIN_MENU;
}

etl::fsm_state_id_t WarmWaterValve::on_event_unknown(etl::imessage_router &sender, const etl::imessage &event)
{
    return STATE_ID;
}

WarmWaterValveSettings::WarmWaterValveSettings()
{
    m_selection = TON;
    m_substate = NONE;
}

etl::fsm_state_id_t WarmWaterValveSettings::on_enter_state()
{
    warm_water_valve_settings(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t WarmWaterValveSettings::on_event(etl::imessage_router &sender, const Next &event)
{
    if (m_substate == NONE)
    {
        m_selection++;
        if (m_selection > SAVE) m_selection = SAVE;
    }

    if (m_substate == ADJ_TON)
    {
        config.warmWaterValveOn++;
    }

    if (m_substate == ADJ_DELAY)
    {
        config.warmWaterValveDelay++;
    }

    warm_water_valve_settings(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t WarmWaterValveSettings::on_event(etl::imessage_router &sender, const Prev &event)
{
    if (m_substate == NONE)
    {
        m_selection--;
        if (m_selection < TON) m_selection = TON;
    }

    if (m_substate == ADJ_TON)
    {
        config.warmWaterValveOn--;
    }

    if (m_substate == ADJ_DELAY)
    {
        config.warmWaterValveDelay--;
    }


    warm_water_valve_settings(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t WarmWaterValveSettings::on_event(etl::imessage_router &sender, const Select &event)
{
    if (m_substate != NONE)
    {
        m_substate = NONE;
        return STATE_ID;
    }

    if (m_selection == TON)
    {
        m_substate = ADJ_TON;
    }

    if (m_selection == DELAY)
    {
        m_substate = ADJ_DELAY;
    }

    if (m_selection == SAVE)
    {
        saveConfig();
        return StateId::WARM_WATER_VALVE;
    }

    return STATE_ID;
}

etl::fsm_state_id_t WarmWaterValveSettings::on_event_unknown(etl::imessage_router &sender, const etl::imessage &event)
{
    return STATE_ID;
}

Louvre::Louvre()
{
    m_selection = AUTO;
}

etl::fsm_state_id_t Louvre::on_enter_state()
{
    louvre(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t Louvre::on_event(etl::imessage_router &sender, const Next &event)
{
    m_selection++;
    if (m_selection > SETTINGS) m_selection = SETTINGS;

    louvre(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t Louvre::on_event(etl::imessage_router &sender, const Prev &event)
{
    m_selection--;
    if (m_selection < ON) m_selection = ON;

    louvre(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t Louvre::on_event(etl::imessage_router &sender, const Select &event)
{
    if (m_selection == ON)
    {
        config.louvre = Config::ON;
    }

    if (m_selection == OFF)
    {
        config.louvre = Config::OFF;
    }

    if (m_selection == AUTO)
    {
        config.louvre = Config::AUTO;
    }

    if (m_selection == SETTINGS)
    {
        return StateId::LOUVRE_SETTINGS;
    }

    return StateId::MAIN_MENU;
}

etl::fsm_state_id_t Louvre::on_event_unknown(etl::imessage_router &sender, const etl::imessage &event)
{
    return STATE_ID;
}

LouvreSettings::LouvreSettings()
{
    m_selection = TON;
    m_substate = NONE;
}

etl::fsm_state_id_t LouvreSettings::on_enter_state()
{
    louvre_settings(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t LouvreSettings::on_event(etl::imessage_router &sender, const Next &event)
{
    if (m_substate == NONE)
    {
        m_selection++;
        if (m_selection > SAVE) m_selection = SAVE;
    }

    if (m_substate == ADJ_TON)
    {
        config.louvreTempClose++;
    }

    if (m_substate == ADJ_TOFF)
    {
        config.louvreTempOpen++;
    }

    louvre_settings(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t LouvreSettings::on_event(etl::imessage_router &sender, const Prev &event)
{
    if (m_substate == NONE)
    {
        m_selection--;
        if (m_selection < TON) m_selection = TON;
    }

    if (m_substate == ADJ_TON)
    {
        config.louvreTempClose--;
    }

    if (m_substate == ADJ_TOFF)
    {
        config.louvreTempOpen--;
    }

    louvre_settings(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t LouvreSettings::on_event(etl::imessage_router &sender, const Select &event)
{
    if (m_substate != NONE)
    {
        m_substate = NONE;
        return StateId::LOUVRE_SETTINGS;
    }

    if (m_selection == TON)
    {
        m_substate = ADJ_TON;
    }

    if (m_selection == TOFF)
    {
        m_substate = ADJ_TOFF;
    }

    if (m_selection == SAVE)
    {
        saveConfig();
        return StateId::LOUVRE;
    }


    return StateId::LOUVRE_SETTINGS;
}

etl::fsm_state_id_t LouvreSettings::on_event_unknown(etl::imessage_router &sender, const etl::imessage &event)
{
    return STATE_ID;
}

RegulatorValve::RegulatorValve()
{
    m_selection = AUTO;
}

etl::fsm_state_id_t RegulatorValve::on_enter_state()
{
    regulator_valve(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t RegulatorValve::on_event(etl::imessage_router &sender, const Next &event)
{
    m_selection++;
    if (m_selection > SETTINGS) m_selection = SETTINGS;

    regulator_valve(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t RegulatorValve::on_event(etl::imessage_router &sender, const Prev &event)
{
    m_selection--;
    if (m_selection < OPEN) m_selection = OPEN;

    regulator_valve(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t RegulatorValve::on_event(etl::imessage_router &sender, const Select &event)
{
    enum RelaisState
    {
        ON = 0,
        OFF = 1,
    };

    static bool op = false;


    if (m_selection == OPEN)
    {
        config.regulatorValve = Config::RV_MANUAL;

        if (!op)
        {
            relMixerOpen = ON;
            relMixerClose = OFF;
            op = true;
        }
        else
        {
            relMixerOpen = OFF;
            relMixerClose = OFF;
            op = false;
        }
        return STATE_ID;
    }
    else if (m_selection == CLOSE)
    {
        config.regulatorValve = Config::RV_MANUAL;

        if (!op)
        {
            relMixerOpen = OFF;
            relMixerClose = ON;
            op = true;
        }
        else
        {
            relMixerOpen = OFF;
            relMixerClose = OFF;
            op = false;
        }
        return STATE_ID;
    }

    if (m_selection == BACK)
    {
        return StateId::MAIN_MENU;
    }

    if (m_selection == AUTO)
    {
        config.regulatorValve = Config::AUTO;
    }

    if (m_selection == SETTINGS)
    {
        return StateId::REGULATOR_VALVE_SETTINGS;
    }

    return StateId::MAIN_MENU;
}

etl::fsm_state_id_t RegulatorValve::on_event_unknown(etl::imessage_router &sender, const etl::imessage &event)
{
    return STATE_ID;
}

RegulatorValveSettings::RegulatorValveSettings()
{
    m_selection = TON;
    m_substate = NONE;
}

etl::fsm_state_id_t RegulatorValveSettings::on_enter_state()
{
    regulator_valve_settings(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t RegulatorValveSettings::on_event(etl::imessage_router &sender, const Next &event)
{
    if (m_substate == NONE)
    {
        m_selection++;
        if (m_selection > SAVE) m_selection = SAVE;
    }

    if (m_substate == ADJ_TON)
    {
        config.regulatorValveOn++;
    }

    regulator_valve_settings(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t RegulatorValveSettings::on_event(etl::imessage_router &sender, const Prev &event)
{
    if (m_substate == NONE)
    {
        m_selection--;
        if (m_selection < TON) m_selection = TON;
    }

    if (m_substate == ADJ_TON)
    {
        config.regulatorValveOn--;
    }

    regulator_valve_settings(m_selection);
    return STATE_ID;
}

etl::fsm_state_id_t RegulatorValveSettings::on_event(etl::imessage_router &sender, const Select &event)
{
    if (m_substate != NONE)
    {
        m_substate = NONE;
        return STATE_ID;
    }

    if (m_selection == TON)
    {
        m_substate = ADJ_TON;
    }

    if (m_selection == SAVE)
    {
        saveConfig();
        return StateId::REGULATOR_VALVE;
    }

    return STATE_ID;
}

etl::fsm_state_id_t RegulatorValveSettings::on_event_unknown(etl::imessage_router &sender, const etl::imessage &event)
{
    return STATE_ID;
}

ValuesState::ValuesState()
{
}

etl::fsm_state_id_t ValuesState::on_enter_state()
{
    sensor_values();
    return STATE_ID;
}

etl::fsm_state_id_t ValuesState::on_event(etl::imessage_router &sender, const Select &event)
{
    return StateId::MAIN_MENU;
}

etl::fsm_state_id_t ValuesState::on_event(etl::imessage_router &sender, const NewValue &event)
{
    sensor_values();
    return STATE_ID;
}

etl::fsm_state_id_t ValuesState::on_event_unknown(etl::imessage_router &sender, const etl::imessage &event)
{
    return STATE_ID;
}
