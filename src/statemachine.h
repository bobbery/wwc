#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

#include "ETL/fsm.h"


const etl::message_router_id_t HEATER_CONTROL = 0;

struct EventId
{
  enum
  {
    NEXT,
    PREV,
    SELECT,
    NEW_VALUE,
  };
};

class Next : public etl::message<EventId::NEXT>
{
};

class Prev : public etl::message<EventId::PREV>
{
};

class Select : public etl::message<EventId::SELECT>
{
};

class NewValue : public etl::message<EventId::NEW_VALUE>
{
};


struct StateId
{
  enum
  {
    MAIN_MENU,
    SOLAR_PUMP,
    SOLAR_PUMP_SETTINGS,
    WARM_WATER_VALVE,
    WARM_WATER_VALVE_SETTINGS,
    LOUVRE,
    LOUVRE_SETTINGS,
    REGULATOR_VALVE,
    REGULATOR_VALVE_SETTINGS,
    VALUES,
    NUMBER_OF_STATES
  };
};


class HeaterControl : public etl::fsm
{
public:

  HeaterControl(etl::ifsm_state** p_states, size_t size)
    : fsm(HEATER_CONTROL)
  {
    set_states(p_states, size);
  }
};

class MainMenu : public etl::fsm_state<HeaterControl, MainMenu, StateId::MAIN_MENU, Next, Prev, Select, NewValue>
{
public:

    enum Selection
    {
        SOLAR_PUMP,
        REGULATOR_VALVE,
        WARM_WATER_VALVE,
        LOUVRE,
        VALUES
    };

    int8_t m_selection;

    MainMenu();

    virtual etl::fsm_state_id_t on_enter_state();
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Next& event);
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Prev& event);
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Select& event);
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const NewValue& event);
    etl::fsm_state_id_t on_event_unknown(etl::imessage_router& sender, const etl::imessage& event);
};


class SolarPump : public etl::fsm_state<HeaterControl, SolarPump, StateId::SOLAR_PUMP, Next, Prev, Select>
{
public:

    enum Selection
    {
        ON,
        OFF,
        AUTO,
        SETTINGS
    };

    int8_t m_selection;

    SolarPump();

    virtual etl::fsm_state_id_t on_enter_state();
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Next& event);
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Prev& event);
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Select& event);
    etl::fsm_state_id_t on_event_unknown(etl::imessage_router& sender, const etl::imessage& event);
};


class SolarPumpSettings : public etl::fsm_state<HeaterControl, SolarPumpSettings, StateId::SOLAR_PUMP_SETTINGS, Next, Prev, Select>
{
public:

    enum Selection
    {
        TON,
        TOFF,
        SAVE
    };

    enum Substate
    {
        NONE,
        ADJ_TON,
        ADJ_TOFF
    };

    int8_t m_selection;
    Substate m_substate;

    SolarPumpSettings();

    virtual etl::fsm_state_id_t on_enter_state();
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Next& event);
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Prev& event);
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Select& event);
    etl::fsm_state_id_t on_event_unknown(etl::imessage_router& sender, const etl::imessage& event);
};


class WarmWaterValve : public etl::fsm_state<HeaterControl, WarmWaterValve, StateId::WARM_WATER_VALVE, Next, Prev, Select>
{
public:

    enum Selection
    {
        ON,
        OFF,
        AUTO,
        SETTINGS
    };

    int8_t m_selection;

    WarmWaterValve();

    virtual etl::fsm_state_id_t on_enter_state();
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Next& event);
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Prev& event);
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Select& event);
    etl::fsm_state_id_t on_event_unknown(etl::imessage_router& sender, const etl::imessage& event);
};


class WarmWaterValveSettings : public etl::fsm_state<HeaterControl, WarmWaterValveSettings, StateId::WARM_WATER_VALVE_SETTINGS, Next, Prev, Select>
{
public:

    enum Selection
    {
        TON,
        DELAY,
        SAVE
    };

    enum Substate
    {
        NONE,
        ADJ_DELAY,
        ADJ_TON
    };


    int8_t m_selection;
    Substate m_substate;

    WarmWaterValveSettings();

    virtual etl::fsm_state_id_t on_enter_state();
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Next& event);
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Prev& event);
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Select& event);
    etl::fsm_state_id_t on_event_unknown(etl::imessage_router& sender, const etl::imessage& event);
};

//===========================================================================================================================



class Louvre : public etl::fsm_state<HeaterControl, Louvre, StateId::LOUVRE, Next, Prev, Select>
{
public:

    enum Selection
    {
        ON,
        OFF,
        AUTO,
        SETTINGS
    };

    int8_t m_selection;

    Louvre();

    virtual etl::fsm_state_id_t on_enter_state();
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Next& event);
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Prev& event);
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Select& event);
    etl::fsm_state_id_t on_event_unknown(etl::imessage_router& sender, const etl::imessage& event);
};


class LouvreSettings : public etl::fsm_state<HeaterControl, LouvreSettings, StateId::LOUVRE_SETTINGS, Next, Prev, Select>
{
public:

    enum Selection
    {
        TON,
        TOFF,
        SAVE
    };

    enum Substate
    {
        NONE,
        ADJ_TON,
        ADJ_TOFF
    };

    int8_t m_selection;
    Substate m_substate;

    LouvreSettings();

    virtual etl::fsm_state_id_t on_enter_state();

    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Next& event);
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Prev& event);
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Select& event);
    etl::fsm_state_id_t on_event_unknown(etl::imessage_router& sender, const etl::imessage& event);
};




class RegulatorValve : public etl::fsm_state<HeaterControl, RegulatorValve, StateId::REGULATOR_VALVE, Next, Prev, Select>
{
public:

    enum Selection
    {
        OPEN,
        CLOSE,
        BACK,
        AUTO,
        SETTINGS
    };

    int8_t m_selection;

    RegulatorValve();

    virtual etl::fsm_state_id_t on_enter_state();
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Next& event);
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Prev& event);
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Select& event);
    etl::fsm_state_id_t on_event_unknown(etl::imessage_router& sender, const etl::imessage& event);
};

class RegulatorValveSettings : public etl::fsm_state<HeaterControl, RegulatorValveSettings, StateId::REGULATOR_VALVE_SETTINGS, Next, Prev, Select>
{
public:

    enum Selection
    {
        TON,
        SAVE
    };

    enum Substate
    {
        NONE,
        ADJ_TON
    };

    int8_t m_selection;
    Substate m_substate;

    RegulatorValveSettings();

    virtual etl::fsm_state_id_t on_enter_state();
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Next& event);
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Prev& event);
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Select& event);
    etl::fsm_state_id_t on_event_unknown(etl::imessage_router& sender, const etl::imessage& event);
};

class ValuesState : public etl::fsm_state<HeaterControl, ValuesState, StateId::VALUES, Select, NewValue>
{
public:
    ValuesState();
    virtual etl::fsm_state_id_t on_enter_state();
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const Select& event);
    etl::fsm_state_id_t on_event(etl::imessage_router& sender, const NewValue& event);
    etl::fsm_state_id_t on_event_unknown(etl::imessage_router& sender, const etl::imessage& event);
};




#endif
