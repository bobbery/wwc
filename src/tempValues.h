#ifndef TEMP_VALUES_H
#define TEMP_VALUES_H

#include <stdint.h>

struct TempValues
{
    int16_t kollektor;
    int16_t speicher;
    int16_t mischer;
    uint32_t tacho;
    
    bool solarPumpOn;
    bool warmWaterValveOn;
    bool regulatorValveOpen;
    bool louvreClosed;
    bool louvreOpen;
};

#endif
