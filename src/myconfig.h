#ifndef GLOBALS_H
#define GLOBALS_H

#include "mbed.h"

struct Config
{
    enum State
    {
        ON,  // or OPEN
        OFF, // or CLOSE
        AUTO,
        RV_MANUAL
    };

    Config()
    {

    }

    State solarPump;
    int8_t solarPumpDeltaOn;
    int8_t solarPumpDeltaOff;

    State regulatorValve;
    int8_t regulatorValveOn;

    State warmWaterValve;
    int8_t warmWaterValveOn; // on when bigger
    int8_t warmWaterValveDelay; // on when bigger

    State louvre;
    int8_t louvreTempClose;
    int8_t louvreTempOpen;
};

extern Config config;
extern bool shallSaveConfig;

const uint32_t ADDRESS = 0x080E0000;

static inline void loadConfig()
{
    FlashIAP iap;
    iap.init();
    iap.read(&config, ADDRESS, sizeof(config));
}

static inline void saveConfig()
{
    shallSaveConfig = true;
}

static inline void storeConfigWhenNecessary()
{
    if (shallSaveConfig)
    {
        shallSaveConfig = false;
        FlashIAP iap;
        iap.init();
        iap.erase(ADDRESS, iap.get_sector_size(ADDRESS));
        iap.program(&config, ADDRESS, sizeof(config));
    }
}



#endif
